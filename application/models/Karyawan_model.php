<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_model extends CI_Model
{
	//panggil nama table
	private $_table = "karyawan";
	
	public function tampilDataKaryawan()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	
	}
	
	public function tampilDataKaryawan2()
	{
		$query = $this->db->query("SELECT * FROM karyawan WHERE flag = 1");
		return $query->result();
	
	}
	
	public function tampilDataKaryawan3()
	{
		$this->db->select('*');
		$this->db->order_by('nik', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function save()
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabung = $thn. "-" . $bln. "-" . $tgl;
		
		$data['nik']				=$this->input->post('nik');
		$data['nama_lengkap']		=$this->input->post('nama');
		$data['tempat_lahir']		=$this->input->post('tempatlahir');
		$data['tgl_lahir']			=$tgl_gabung;
		$data['jenis_kelamin']		=$this->input->post('jenis_kelamin');
		$data['alamat']				=$this->input->post('alamat');
		$data['telp']				=$this->input->post('telp');
		$data['kode_jabatan']		=$this->input->post('kode_jabatan');
		$data['flag']				=1;
		$this->db->insert($this->_table, $data);
	}
	
	public function detail($nik)
	{
		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
}
