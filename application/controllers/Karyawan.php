<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("karyawan_model");
		$this->load->model("jabatan_model");
	}
	
	public function index()
	{
		$this->listkaryawan();
	}
	
	public function listkaryawan()
	{
		$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		$this->load->view('home_karyawan', $data);
	}
	
	public function inputkaryawan()
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		
		if(!empty($_REQUEST)) {
			$m_karyawan = $this->karyawan_model;
			$m_karyawan->save();
			redirect("karyawan/index", "refresh");
			}
		
		$this->load->view('input_karyawan', $data);
	}
	
	public function detailkaryawan($nik)
	{
		$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
		$this->load->view('detail_karyawan', $data);
	}
	
}