<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_barang extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("jenis_barang_model");
	}
	
	public function index()
	{
		$this->listjenisbarang();
	}
	
	public function listjenisbarang()
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDataJenisBarang();
		$this->load->view('home_jenis_barang', $data);
	}
	
	public function inputjenisbarang()
	
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDataJenisBarang();
		if (!empty($_REQUEST)) {
			$m_jenis_barang = $this->jenis_barang_model;
			$m_jenis_barang->save();
			redirect("jenis_barang/index", "refresh");
		}
			
		$this->load->view('input_jenis_barang');
	}
	public function detailjenisbarang($kode_jenis)
	
	{
		$data['detail_jenis_barang'] = $this->jenis_barang_model->detail($kode_jenis);
		$this->load->view('detail_jenis_barang', $data);
	}

	
}