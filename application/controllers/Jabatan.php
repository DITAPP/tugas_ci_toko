<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("jabatan_model");
	}
	
	public function index()
	{
		$this->listjabatan();
	}
	
	public function listjabatan()
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$this->load->view('home_jabatan', $data);
	}
	
	public function inputjabatan()
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		
		if(!empty($_REQUEST)) {
			$m_jabatan = $this->jabatan_model;
			$m_jabatan->save();
			redirect("jabatan/index", "refresh");
			}
		
		$this->load->view('input_jabatan', $data);
	}
	public function detailjabatan($kode_jabatan)
	{
		$data['detail_jabatan'] = $this->jabatan_model->detail($kode_jabatan);
		$this->load->view('detail_jabatan', $data);
	}
	
}