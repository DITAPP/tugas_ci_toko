<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("barang_model");
		$this->load->model("jenis_barang_model");
		
	}
	
	public function index()
	{
		$this->listbarang();
	}
	
	public function listbarang()
	{
		$data['data_barang'] = $this->barang_model->tampilDataBarang();
		$this->load->view('home_barang', $data);
	}
	
	public function inputbarang()
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDataJenisBarang();
		
		if(!empty($_REQUEST)) {
			$m_barang = $this->barang_model;
			$m_barang->save();
			redirect("barang/index", "refresh");
			}
		
		$this->load->view('input_barang', $data);
	}
	
	public function detailbarang($kode_barang)
	{
		$data['detail_barang'] = $this->barang_model->detail($kode_barang);
		$this->load->view('detail_barang', $data);
	}
	
}